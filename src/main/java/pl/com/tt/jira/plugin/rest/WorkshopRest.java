package pl.com.tt.jira.plugin.rest;

import pl.com.tt.jira.plugin.bean.IssueData;
import pl.com.tt.jira.plugin.bean.WorkshopIssueBean;
import pl.com.tt.jira.plugin.service.WorkshopIssueService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Named
@Path("/workshop")
public class WorkshopRest {

    private final WorkshopIssueService workshopIssueService;

    @Inject
    public WorkshopRest(WorkshopIssueService workshopIssueService) {
        this.workshopIssueService = workshopIssueService;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getIssueDataByIssueKey(@QueryParam("issueKey") String issueKey) {
        IssueData issueData = workshopIssueService.getIssueDataByIssueKey(issueKey);
        return Response.ok(issueData).build();
    }

    @POST
    @Path("/model")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response sendWorkshopModelToServer(WorkshopIssueBean workshopIssueBean) {
        String creationStatus = workshopIssueService.createNewIssue(workshopIssueBean);
        return Response.ok(creationStatus).build();
    }

    @GET
    @Path("/flights/{issueKey}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getFlights(@PathParam("issueKey") String issueKey) {
        String flights = workshopIssueService.fetchFlights(issueKey);
        return Response.ok(flights).build();
    }


}
