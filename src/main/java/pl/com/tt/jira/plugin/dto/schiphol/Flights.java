package pl.com.tt.jira.plugin.dto.schiphol;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class Flights {

    private String lastUpdatedAt;
    private String actualLandingTime;
    private String actualOffBlockTime;
    private String aircraftRegistration;

    private AircraftType aircraftType;
    private BaggageClaim baggageClaim;
    private Codeshares codeshares;
    @JsonProperty("checkinAllocations;")
    private JsonNode checkinAllocations;

    private String estimatedLandingTime;
    private String expectedTimeBoarding;
    private String expectedTimeGateClosing;
    private String expectedTimeGateOpen;
    private String expectedTimeOnBelt;

    private String expectedSecurityFilter;
    private String flightDirection;
    private String flightName;
    private int flightNumber;
    private String gate;
    private String pier;
    private String id;
    private boolean isOperationalFlight;
    private String mainFlight;
    private String prefixIATA;
    private String prefixICAO;
    private int airlineCode;
    private String publicEstimatedOffBlockTime;
    private PublicFlightState publicFlightState;

    private Route route;
    private String scheduleDateTime;
    private DateTime scheduleDate;
    private String scheduleTime;
    private String serviceType;
    private int terminal;
    private String transferPositions;
    private String schemaVersion;

}
