package pl.com.tt.jira.plugin.actions;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import webwork.action.Action;

public class WorkshopInternshipModule extends JiraWebActionSupport {

    @Override
    public String doDefault() {
        return Action.SUCCESS;
    }

}
