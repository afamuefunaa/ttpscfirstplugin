package pl.com.tt.jira.plugin.service;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import pl.com.tt.jira.plugin.bean.IssueData;
import pl.com.tt.jira.plugin.bean.WorkshopIssueBean;
import pl.com.tt.jira.plugin.dto.schiphol.PublicFlights;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Named
public class WorkshopIssueServiceImpl implements WorkshopIssueService {

    private final IssueManager issueManager;
    private final IssueFactory issueFactory;
    private final ProjectManager projectManager;
    private final IssueTypeManager issueTypeManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final CustomFieldManager customFieldManager;


    @Inject
    public WorkshopIssueServiceImpl(@ComponentImport IssueManager issueManager,
                                    @ComponentImport IssueFactory issueFactory,
                                    @ComponentImport ProjectManager projectManager,
                                    @ComponentImport IssueTypeManager issueTypeManager,
                                    @ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
                                    @ComponentImport CustomFieldManager customFieldManager) {
        this.issueManager = issueManager;
        this.issueFactory = issueFactory;
        this.projectManager = projectManager;
        this.issueTypeManager = issueTypeManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public IssueData getIssueDataByIssueKey(String issueKey) {
        Issue issue = issueManager.getIssueByCurrentKey(issueKey);
        return new IssueData(issue.getSummary(), issue.getDescription());
    }

    @Override
    public String createNewIssue(WorkshopIssueBean workshopIssueBean) {
        Project project = projectManager.getProjectByCurrentKey(workshopIssueBean.getProjectKey());
        IssueType issueType = issueTypeManager.getIssueTypes().stream()
                .filter(issueTypeFilter -> issueTypeFilter.getName().equals(workshopIssueBean.getIssueType()))
                .findAny().orElse(null);

        if (issueType == null) {
            return "No issue type with corresponding name was found";
        }

        ApplicationUser applicationUser = jiraAuthenticationContext.getLoggedInUser();

        try {
            MutableIssue mutableIssue = issueFactory.getIssue();
            mutableIssue.setSummary(workshopIssueBean.getSummary());
            mutableIssue.setDescription(workshopIssueBean.getDescription());
            mutableIssue.setIssueTypeId(issueType.getId());
            mutableIssue.setReporter(applicationUser);
            mutableIssue.setProjectId(project.getId());
            issueManager.createIssueObject(applicationUser, mutableIssue);
        } catch (CreateException createException) {
            return "There was an error while creating issue object, " + createException.getMessage();
        }

        return "Creating issue was successful!";
    }





    @Override
    public String fetchFlights(String issueKey) {

        String startDelegationId = "customfield_10000";
        String endDelegationId = "customfield_10001";

        Issue issue = issueManager.getIssueByCurrentKey(issueKey);

        CustomField startDelegationCF  = customFieldManager.getCustomFieldObject(startDelegationId);
        CustomField endDelegationCF  = customFieldManager.getCustomFieldObject(endDelegationId);

        Timestamp startDelegationTimestamp = (Timestamp) issue.getCustomFieldValue(startDelegationCF);
        LocalDate startDelegation = startDelegationTimestamp.toLocalDateTime().toLocalDate();

        Timestamp endDelegationTimestamp = (Timestamp) issue.getCustomFieldValue(endDelegationCF) ;
        LocalDate endDelegation = endDelegationTimestamp.toLocalDateTime().toLocalDate();


        MutableIssue mutableIssue = issueManager.getIssueByCurrentKey(issueKey);
        ApplicationUser currentUser = jiraAuthenticationContext.getLoggedInUser();

        try {
            URL url = new URL("https://api.schiphol.nl/public-flights/flights?fromScheduleDate="+
                    startDelegation+"&toScheduleDate="+ endDelegation);

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Accept","application/json");
            httpURLConnection.setRequestProperty("app_id","f1600ada");
            httpURLConnection.setRequestProperty("app_key","3c267aeeaebc3272dc5fbdf417612a4d");
            httpURLConnection.setRequestProperty("ResourceVersion","v4");
            httpURLConnection.setConnectTimeout(50000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setRequestMethod("GET");

            ObjectMapper objectMapper = new ObjectMapper();
            PublicFlights flights = objectMapper.readValue(httpURLConnection.getInputStream(), PublicFlights.class);

            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            mutableIssue.setDescription(ow.writeValueAsString(flights));
            issueManager.updateIssue(currentUser, mutableIssue, EventDispatchOption.ISSUE_UPDATED, false);
            return ow.writeValueAsString(flights);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
