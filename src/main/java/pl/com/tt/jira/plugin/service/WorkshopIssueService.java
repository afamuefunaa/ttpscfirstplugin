package pl.com.tt.jira.plugin.service;

import pl.com.tt.jira.plugin.bean.IssueData;
import pl.com.tt.jira.plugin.bean.WorkshopIssueBean;

public interface WorkshopIssueService {
    IssueData getIssueDataByIssueKey(String issueKey);
    String createNewIssue(WorkshopIssueBean workshopIssueBean);
    String fetchFlights(String issueKey);
}
